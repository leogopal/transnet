## Transnet Laravel Application

This is the Laravel based application for the transnet app.

### Installation

- Clone this repository.
- Run `composer install`.
- Copy .env.example to .env and change the details accordingly.
- `php artisan key:generate`.
- `npm install`.
- `php artisan migrate`.
- `php artisan db:seed`.
- `npm run development` for development.
- `php artisan storage:link`.

Dummy login credentials of an Admin:
username: admin@admin.com
password: secret